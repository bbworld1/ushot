# ushot: the ultimate screenshot CLI utility.

ushot is a simple yet powerful screenshot CLI utility for Linux.

## Features

Ushot provides a simple, powerful screenshot CLI syntax that
works on both X11 and Wayland.

Note that Ushot is not in itself a screenshot utility; rather,
it brings together various utilities under a unified CLI interface
to make it easier to work with. So technically you don't need this at all.
But hey, Ushot itself is only like a couple dozen lines, so why not? :)

### Regular Features (./install.sh --regular) (default option)
Whole screen:
```sh
ushot
```

Window:
```sh
ushot --window
```

Region:
```sh
ushot --region
```

Output to a file (default: stdout):
```sh
ushot output.png
```
Output format automatically detected from file extension. JPG outputted by default.
Shell substitution will work (e.g. `ushot "~/Pictures/Screenshot at $(date).jpg"`).

Copy to clipboard:
```sh
ushot --clipboard
```

### Extra Features (./install.sh --phat)
TODO: These are not yet implemented. Hold tight!

Downscale to size:
```sh
ushot --downscale-to 20K # Downscale image to a size that fits within the specified size (resolution will be lost)
```

Compress to size:
```sh
ushot --compress-to 20K # Compress to size (works only on JPEG, artifacts will definitely exist)
```

## Installation
As you can probably guess by the above examples,
to install this use the provided `install.sh` script:

`./install.sh`

to install the default "regular" profile with both Wayland and X11 support.
Alternatively, if you'd like a lighter or heavier distribution, `--unix-philosophy` will provide
only the very bare minimum (just screenshotting itself), while `--phat` will include extra (perhaps unnecessary) options for manipulating the screenshot after you take it.

You may also choose to install only X11 or Wayland support:

`./install.sh --x11-only` or `./install.sh --wayland-only`

## Contributing
I mean, this repo's pretty simple. You have this README, the license, the install.sh script, and the ushot script itself. Just open an issue and/or submit a merge request like normal.
