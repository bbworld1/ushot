#!/bin/bash
# Script to install Ushot
# To be honest, since Ushot is really only a collection
# of other utilities under a CLI, this is where most of the magic
# happens
set -e

# Define help
function help {
    echo "install.sh usage:"
    echo "  ./install.sh [-h, --help] [--regular, --unix-philosophy, --phat] "
    echo "                                                                   "
    echo "  This script installs ushot, a screenshot CLI interface.          "
    echo "  Options:                                                         "
    echo "                                                                   "
    echo "      -h,--help           prints this help message                 "
    echo "      --regular           installs the regular profile (see README)"
    echo "      --phat              installs all the things                  "
}

# Here we have a massive if-elseif chain for all the different
# package managers
PROFILE="regular"
X11_WAYLAND="both"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --regular)
        PROFILE="regular"
        shift # past argument
        shift # past value
    ;;
    --phat)
        PROFILE="phat"
        shift # past argument
        shift # past value
    ;;
    --x11-only)
        X11_WAYLAND="x11"
        shift # past argument
        shift # past value
    ;;
    --wayland-only)
        X11_WAYLAND="wayland"
        shift # past argument
        shift # past value
    ;;
    --h|--help)
        help
        exit 0
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

echo "SELECTED PROFILE    = ${PROFILE}"
echo "X11/WAYLAND SUPPORT = ${X11_WAYLAND}"

PACKAGES=""

if [ "$X11_WAYLAND" != "x11" ]; then
    # Install wayland packages (not x11 only)
    PACKAGES="${PACKAGES} maim xclip"
fi
if [ "$X11_WAYLAND" != "wayland" ]; then
    # Install wayland packages (not wayland only)
    if ! [ -f "/usr/bin/pacman" ]; then
        git clone --depth 1 https://github.com/emersion/slurp.git
        cd slurp
        meson build
        ninja -C build
        sudo install -m 755 build/slurp /usr/local/bin
        cd ..

        git clone --depth 1 https://github.com/emersion/grim.git
        cd grim
        meson build
        ninja -C build
        sudo install -m 755 build/grim /usr/local/bin
        cd ..
    fi
fi

echo "Detecting package manager..."
if [ -f "/usr/bin/pacman" ]; then
    echo "Pacman (Arch/Manjaro) detected."
    if [ "$X11_WAYLAND" != "wayland" ]; then
        # Install wayland packages (not wayland only)
        PACKAGES="${PACKAGES} grim slurp wl-clipboard"
    fi
    sudo pacman -S $PACKAGES
elif [ -f "/usr/bin/apt" ]; then
    echo "Apt (Debian/Ubuntu) detected."
    sudo apt install $PACKAGES
elif [ -f "/usr/bin/dnf" ]; then
    echo "DNF (Fedora) detected."
    sudo dnf install $PACKAGES
elif [ -f "/usr/bin/emerge" ]; then
    echo "Portage (Gentoo? Respect.) detected."
    sudo emerge $PACKAGES
else
    echo "Sorry, your package manager isn't supported!"
    exit 1
fi

echo "Installing script..."
sudo install -m 755 ushot /usr/local/bin
echo "Finished!"
